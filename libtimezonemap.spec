Name:           libtimezonemap
Version:        0.4.5.3
Release:        1
Summary:        A GTK+3 timezone map widget
License:        GPLv3
URL:            https://launchpad.net/timezonemap
Source0:        http://github.com/dashea/timezonemap/archive/%{version}.tar.gz
#Patch9000:      0001-modify-timezone-Macau-to-Macao.patch

BuildRequires:  glib2-devel gnome-common gobject-introspection-devel gtk3-devel
BuildRequires:  json-glib-devel librsvg2-devel libsoup-devel

%description
libtimezonemap is a time zone map widget for Gtk+. The widget displays a world
map with a highlighted region representing the selected time zone, and the
location can be changed by clicking on the map.

This library is a fork of the of the code from gnome-control-center's datetime
panel, which was itself a fork of Ubiquity's timezone map.

%package        devel
Summary:        Header files, libraries and developer documentatio for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The package contains header files used for building applications that use %{name}.

%package_help

%prep
%autosetup -n timezonemap-%{version} -p1

%build
./autogen.sh
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license COPYING
%doc AUTHORS
%{_libdir}/libtimezonemap.so.*
%{_libdir}/girepository-1.0/TimezoneMap-1.0.typelib
%{_datadir}/libtimezonemap

%files devel
%defattr(-,root,root)
%{_libdir}/libtimezonemap.so
%{_libdir}/pkgconfig/timezonemap.pc
%{_includedir}/timezonemap
%{_datadir}/gir-1.0/TimezoneMap-1.0.gir
%{_datadir}/glade/catalogs/TimezoneMap.xml

%files help
%defattr(-,root,root)
%doc README TODO NEWS ChangeLog 

%changelog
* Wed Mar 13 2024 liweigang <liweiganga@uniontech.com> - 0.4.5.3-1
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: update libtimezonemap version to 0.4.5.3

* Wed Oct 19 2022 yanglu <yanglu72@h-partners.com> - 0.4.5.2-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update libtimezonemap version to 0.4.5.2

* Fri Sep 11 2020 lunankun <lunankun@huawei.com> - 0.4.5.1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix source0 url

* Tue Jan 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.4.5.1-6
- Type:bugfix
- ID:NA
- SUG:reboot
- DESC:modify timezone from Macau to Macao

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.5.1-5
- Package init
